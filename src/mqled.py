import paho.mqtt.client as mqtt

from config import CONFIG
from ledstrip import LEDStrip

import logging
import re

import signal
import sys

logging.basicConfig(format='[%(asctime)s MQLED] %(message)s',
                    datefmt='%H:%M:%S', level=logging.INFO)

led_strip = LEDStrip(CONFIG)

def publish_color(client):
    client.publish("ledcontrol/strip0color", f'R={led_strip.red},G={led_strip.green},B={led_strip.blue}',qos=0,retain=True)
    client.publish("ledcontrol/strip0/hsv", f'H={led_strip.hue},S={led_strip.sat},V={led_strip.value}',qos=0,retain=True)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    logging.info("Connected with result code "+str(rc))
    client.publish("ledcontrol/strip0/status","online",qos=0,retain=True)

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("ledcontrol/strip0")
    client.subscribe("ledcontrol/strip0/set_hsv")
    publish_color(client)

def clamp(value, min_value, max_value):
    return max(min_value, min(value, max_value))
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, message):
    data = str(message.payload.decode("utf-8"))
    logging.info("message received " + data)
    logging.info("message topic=" + message.topic)
    logging.info("message qos=" + str(message.qos))
    logging.info("message retain flag=" + str(message.retain))
    if re.match(".*set_hsv$", message.topic):
        try:
            (h,s,v) = data.split(',')
            logging.info("H=%s S=%s V=%s" % (h,s,v))
            h = float(h) % 1.0
            s = clamp(float(s), 0.0, 1.0)
            v = clamp(float(v), 0.0, 1.0)
            logging.info("=>H=%f S=%f V=%f" % (h,s,v))
            led_strip.set_hsv(h,s,v)
            publish_color(client)
        except ValueError:
            logging.info("Invalid value " + data)
    else:
        try:
            (r,g,b) = data.split(',')
            logging.info("R=%s G=%s B=%s" % (r,g,b))
            led_strip.set(r,g,b)
            publish_color(client)
        except ValueError:
            logging.info("Invalid value " + data)

def on_disconnect():
    logging.info("Disconnect")

client = mqtt.Client("led/strip1")
client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect
client.will_set("ledcontrol/strip0/status","offline",0,retain=True)

client.connect(CONFIG["mq_host"], 1883, 60)

def sigterm_handler(_signo, _stack_frame):
    # Raises SystemExit(0):
    sys.exit(0)

signal.signal(signal.SIGTERM, sigterm_handler)


# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
try:
    client.loop_forever()
except:
    logging.info("Unexpected error: ${0}".format(sys.exc_info()[0]))
    logging.info("The End")
led_strip.teardown()