"""
MQLed - Config
"""
import configparser
import logging
import os


logging.basicConfig(format='[%(asctime)s MQLED] %(message)s',
                    level=logging.INFO)


def ordereddict_to_int(ordered_dict):
    """
    Converts an OrderedDict with unicode values to integers and/or floats (port and pins).
    @param ordered_dict: <OrderedDict> containg MQLED configuration.

    @returns: <OrderedDict> with integers instead of unicode values.
    """
    for key,value in ordered_dict.items():
        if u"." in str(value):
            casting_function = float
        else:
            casting_function = int
        try:
            ordered_dict[key] = casting_function(value)
        except (TypeError, ValueError):
            pass
    return ordered_dict



MQLED_DIR = os.path.dirname(os.path.realpath(__file__))  # The directory we're running in

DEFAULTS = {
    'mq_host': 'localhost',
    'pi_host': 'localhost',
    'pig_port': 8888,  # the port pigpio daemon is listening on for pin control commands

    # Initial default values for your output pins. You can override them in your mqled.conf file
    'red_pin': '14',
    'green_pin': '15',
    'blue_pin': '18',
    'white_pin': '23',
}


config_path = os.path.expanduser(MQLED_DIR + '/mqled.conf')
parser = configparser.ConfigParser(defaults=DEFAULTS)
params = {}

if os.path.exists(config_path):
    logging.info('Using config file: {}'.format(config_path))
    parser.read(config_path)
    params = ordereddict_to_int(parser.defaults())
    config_file_needs_writing = False
else:
    config_file_needs_writing = True
    # No config file exists, give the user a chance to specify their pin configuration
    logging.warn('No config file found. Creating default {} file.'.format(config_path))
    logging.warn('*** Please edit this file as needed. ***')

    # Allow user to customise their pin config
    while True:
        try:  # These will assume the default settings UNLESS you enter a different value
            user_input_red_pin = int(input('RED pin number [{}]:'.format(DEFAULTS["red_pin"])) or DEFAULTS["red_pin"])
            user_input_green_pin = int(input('GREEN pin number [{}]:'.format(DEFAULTS["green_pin"])) or DEFAULTS["green_pin"])
            user_input_blue_pin = int(input('BLUE pin number [{}]:'.format(DEFAULTS["blue_pin"])) or DEFAULTS["blue_pin"])
            user_input_white_pin = int(input('WHITE pin number [{}]:'.format(DEFAULTS["white_pin"])) or DEFAULTS["white_pin"])
        except (ValueError, TypeError):
            logging.warn('*** The input should be an integer ***')
        else:
            DEFAULTS['red_pin'] = user_input_red_pin
            DEFAULTS['green_pin'] = user_input_green_pin
            DEFAULTS['blue_pin'] = user_input_blue_pin
            DEFAULTS['white_pin'] = user_input_white_pin
            dict = {}
            dict[DEFAULTS['red_pin']]=1
            dict[DEFAULTS['blue_pin']]=1
            dict[DEFAULTS['green_pin']]=1
            dict[DEFAULTS['white_pin']]=1
            if len(dict) != 4:
                logging.warn('*** The pin number should be different for all pins. ***')
            else:
                config_file_needs_writing = True
                break

user_pig_port = params.get("pig_port", DEFAULTS["pig_port"])
while True:
    config_is_ok = True
    try:
        port = int(user_pig_port)
    except (ValueError, TypeError):
        logging.warn("*** You have specified an invalid port number the Pigpio daemon ({}) ***".format(DEFAULTS["pig_port"]))
    else:  # Config is fine... carry on
        DEFAULTS["pig_port"] = user_pig_port
        break

    try:
        user_pig_port = int(input('Pigpio daemon port (e.g. 8888) [{}]:'.format(DEFAULTS["pig_port"])) or DEFAULTS["pig_port"])
    except (ValueError, TypeError):
        logging.warn('*** The input should be an integer ***')
    else:
        config_file_needs_writing = True

# Now write the config file if needed
if config_file_needs_writing:
    parser = configparser.ConfigParser(defaults=DEFAULTS)
    with open(config_path, 'w') as f:
        parser.write(f)
    params = ordereddict_to_int(parser.defaults())


CONFIG = params


def get_setting(name, default=None):
    """
    Return the setting from the config. If it doesn't exist,
    will check the defaults dict, and then finally fallback
    to the default.
    :param name:
    :param default:
    :return:
    """
    return params.get(name, DEFAULTS.get(name, default))


get_settings = get_setting
get_config = get_setting
