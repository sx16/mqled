import paho.mqtt.client as mqtt

import logging
from config import CONFIG

import signal
import sys
import re


def set_color(client):
    client.publish("ledcontrol/strip0/set_hsv", '1.0,1.0,1.0',qos=0,retain=False)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    logging.info("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("ledcontrol/strip0color")
    client.subscribe("ledcontrol/strip0/hsv")
    client.subscribe("ledcontrol/strip0/trigger")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, message):
    data = str(message.payload.decode("utf-8"))
    logging.info("message received " + data)
    logging.info("message topic=" + message.topic)
    logging.info("message qos=" + str(message.qos))
    logging.info("message retain flag=" + str(message.retain))
    if re.match(".*trigger$", message.topic):
        set_color(client)
    
def on_disconnect():
    logging.info("Disconnect")

client = mqtt.Client("led/strip1_stress")
client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect

client.connect(CONFIG["mq_host"], 1883, 60)

def sigterm_handler(_signo, _stack_frame):
    # Raises SystemExit(0):
    sys.exit(0)

signal.signal(signal.SIGTERM, sigterm_handler)


try:
    client.loop_forever()
except:
    logging.info("Unexpected error: ${0}".format(sys.exc_info()[0]))
    logging.info("The End")
