#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    RGBW LED strip light control from Raspberry Pi!

"""

from configparser import ConfigParser
import os
import pigpio
from time import sleep
import subprocess
import logging
import colorsys

##### Constants #####

PWM_MAX = 255.0
PWM_MIN = 0.0


#####################


# pigpio_interface = pigpio.pi("192.168.0.33",8888) #We use ONE class instance
def pigpiod_process():
    cmd = 'pgrep pigpiod'

    process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

    if output == '':
        logging.warn('*** [STARTING PIGPIOD] i.e. "sudo pigpiod" ***')
        cmd = 'sudo pigpiod'
        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
    else:
        logging.info('PIGPIOD is running! PID: %s' % output.decode('utf-8').split('\n')[0])


pigpiod_process()


class LEDStrip(object):
    """
    Represents an LED strip
    """
    # Runtime vars
    r = 0.0  # Current value of red channel
    g = 0.0  # Current value of green channel
    b = 0.0  # Current value of blue channel
    w = 0.0  # Current value of white channel
    h = 0.0
    s = 0.0
    v = 0.0
    iface = None
    MQLED_DIR = os.path.dirname(os.path.realpath(__file__))  # The directory we're running in
    state_path = os.path.expanduser(MQLED_DIR + '/state.ini')
    parser = ConfigParser()

    def __init__(self, params, interface=None):
        """
        Initialises the lights
        
        :param params: Dict of settings
        :keyword interface: <pigpio.pi> The RaspberryPi hardware we're talking to!
        """
        red_pin = params.get("red_pin", 27)
        green_pin = params.get("green_pin", 17)
        blue_pin = params.get("blue_pin", 22)
        white_pin = params.get("white_pin", 23)
        pi_host = params.get("pi_host", "localhost")
        pig_port = params.get("pig_port", 8888)


        # Resolve interface - create if not provided!
        need_to_generate_new_interface = False  # Flag to see what we're doing
        if interface is None:
            need_to_generate_new_interface = True
        else:  # Check the interface is connected!
            try:
                iface_host = interface._host
            except AttributeError:
                iface_host = None
            if iface_host is None:
                need_to_generate_new_interface = True
                logging.info("No existing iface host")
            elif pi_host and str(pi_host) != str(iface_host):
                need_to_generate_new_interface = True
                logging.info("iface host different to intended: iface=%s vs pi=%s" % (iface_host, pi_host))
            try:
                iface_port = interface._port
            except AttributeError:
                iface_port = None
            if iface_port is None:
                need_to_generate_new_interface = True
            elif pig_port and str(pig_port) != str(iface_port):
                need_to_generate_new_interface = True
                logging.info("iface port different to intended: iface=%s vs pi=%s" % (iface_port, pig_port))
            try:
                iface_connected = interface.connected
            except AttributeError:
                iface_connected = False
            if not iface_connected:
                logging.info("iface not connected!")
                need_to_generate_new_interface = True
        if need_to_generate_new_interface:
            self.iface = self.generate_new_interface(params)
        else:
            self.iface = interface

        # Set vars
        self._red_pin = self.pin_lim(red_pin)
        self._green_pin = self.pin_lim(green_pin)
        self._blue_pin = self.pin_lim(blue_pin)
        self._white_pin = self.pin_lim(white_pin)

        self.parser.add_section('state')

        if os.path.exists(self.state_path):
            self.parser.read(self.state_path)
            try:
                self.r = self.parser.getint('state', 'r')
                self.g = self.parser.getint('state', 'g')
                self.b = self.parser.getint('state', 'b')
                self.h = self.parser.getfloat('state', 'h')
                self.s = self.parser.getfloat('state', 's')
                self.v = self.parser.getfloat('state', 'v')
            except:
                pass
        self.set_rgb(self.r, self.g, self.b)

    @classmethod
    def lim(cls, lower=PWM_MIN, upper=PWM_MAX, value=None, less_than_lower_default=None, greater_than_upper_default=None):
        """
        Checks that the value specified is between the given values, or returns default
        """
        # Sanitise inputs
        if less_than_lower_default is None:
            less_than_lower_default = lower
        if greater_than_upper_default is None:
            greater_than_upper_default = upper
        if not (less_than_lower_default >= lower and greater_than_upper_default <= upper):
            raise Exception("LEDStrip.lim(): Defaults %s,%s are not within %s - %s" % (less_than_lower_default, greater_than_upper_default, lower, upper))
        if value is None:
            return less_than_lower_default

        # Test values
        try:
            if value < lower:
                logging.warn(" LEDStrip.lim(): Value %s is less than lower limit %s. Setting to %s." % (value, lower, less_than_lower_default))
                return float(less_than_lower_default)
            if value > upper:
                logging.warn(" LEDStrip.lim(): Value %s is greater than upper limit %s. Setting to %s" % (value, upper, greater_than_upper_default))
                return float(greater_than_upper_default)
        except (ValueError, TypeError, AttributeError):
            return float(less_than_lower_default)
        return float(value)

    @classmethod
    def int_lim(cls, lower=PWM_MIN, upper=PWM_MAX, value=None, less_than_lower_default=None, greater_than_upper_default=None):
        """
        Checks that the value specified is between the given values, or returns default. Always an integer.
        """
        out_float = cls.lim(lower, upper, value, less_than_lower_default, greater_than_upper_default)
        return int(round(out_float))

    @classmethod
    def pin_lim(cls, value):
        """
        Checks that the pin specified is between valid ranges
        """
        return cls.int_lim(lower=0, upper=27, value=value, less_than_lower_default=27, greater_than_upper_default=27)

    def __unicode__(self):
        """
        Print current colours as unicode
        """
        return "{},{},{},{}".format(*self.rgbw)

    @property
    def red(self):
        """
        Red
        """
        r = self.r
        return int(r)

    @property
    def green(self):
        """
        Green
        """
        g = self.g
        return int(g)

    @property
    def blue(self):
        """
        Blue
        """
        b = self.b
        return int(b)

    @property
    def white(self):
        """
        White
        """
        w = self.w
        return int(w)
    
    @property
    def hue(self):
        h = self.h
        return h
    
    @property
    def sat(self):
        s = self.s
        return s
    
    @property
    def value(self):
        v = self.v
        return v   

    @property
    def hsv(self):
        return (self.h, self.s, self.v)

    @property
    def rgbw(self):
        """
        RGBW
        """
        return (self.red, self.green, self.blue, self.w)

    def generate_new_interface(self, params):
        """
        Builds a new interface, stores it in self.iface
        """
        # Kill existing iface
        try:
            self.iface.stop()
        except (AttributeError, IOError):
            pass
        self.iface = pigpio.pi(params['pi_host'], params['pig_port'])
        return self.iface

    def set_led(self, pin, value=0):
        """
        Sets the LED pin to the specified value
        
        @param pin: <int> The pin to change
        @param value: <int> The value to set it to
        """
        value = self.int_lim(lower=PWM_MIN, upper=PWM_MAX, value=value)  # Standardise the value to our correct range
        if self.iface.connected:
            try:
                self.iface.set_PWM_dutycycle(pin, value)
            except (AttributeError, IOError):
                logging.error(" Cannot output to pins. PWM of pin #%s would be %s" % (pin, value))
        else:
            logging.error(" Interface not connected. Cannot output to pins. PWM of pin #%s would be %s" % (pin, value))
        return value

    def read_led(self, pin):
        """
        Reads the current LED pin value, sets our internal pointer to its value
        
        @param pin: <int> The pin to read 
        """
        value = 0  # Default to nowt
        if self.iface.connected:
            try:
                value = self.iface.get_PWM_dutycycle(pin)
            except (AttributeError, IOError, pigpio.error):
                logging.error(" Cannot read PWM of pin #%s" % (pin,))
        else:
            logging.error(" Interface not connected. Cannot read PWM of pin #%s." % (pin))
        return value

    def read_rgbw(self):
        """
        Reads the LED pin values (raw values)
        """
        r = self.read_led(self._red_pin)
        g = self.read_led(self._green_pin)
        b = self.read_led(self._blue_pin)
        w = self.read_led(self._white_pin)
        return (r, g, b, w)

    def set_red(self, value=0):
        """
        Sets the red LED to value
        """
        self.r = self.set_led(self._red_pin, value)
        return self.red

    def set_green(self, value=0):
        """
        Sets the green LED to value 
        """
        self.g = self.set_led(self._green_pin, value)
        return self.green

    def set_blue(self, value=0):
        """
        Sets the blue LED to value
        """
        self.b = self.set_led(self._blue_pin, value)
        return self.blue

    def set_white(self, value=0):
        """
        Sets the white LED to value
        """
        self.w = self.set_led(self._white_pin, value)
        return self.w

    def set_rgb(self, r=0, g=0, b=0):
        """
        Sets the LED array to rgb
        @return: (r,g,b)
        """
        r = self.set_red(r)
        g = self.set_green(g)
        b = self.set_blue(b)

        self.set_white(min(r,g,b))
        return (r, g, b)

    def hsv2rgb(self, h,s,v):
        return tuple(round(i * 255) for i in colorsys.hsv_to_rgb(h,s,v))
        
    def set_hsv(self, h=0, s=0, v=0):
        self.h = h
        self.s = s
        self.v = v
        self.set(self.hsv2rgb(h,s,v))
        return (h,s,v)

    def fade_to_rgb(self, r=0, g=0, b=0, fade=300):
        """
        Fades to the rgb values over the specified time period (in milliseconds)
        Human perception notices things slower than 50Hz (20ms)
        
        @keyword fade: <float> if provided, will make the colour transition smooth over the specified period of time
        """

        # Now we'll have the correct init values!!!
        init_r = self.red
        init_g = self.green
        init_b = self.blue
        gap_r = r - init_r
        gap_g = g - init_g
        gap_b = b - init_b
        n_steps = int(float(fade) / 20.0)  # 50Hz = 20 milliseconds

        for step in range(0, n_steps):
            fractional_progress = float(step) / n_steps
            cur_r = init_r + (gap_r * fractional_progress)
            cur_g = init_g + (gap_g * fractional_progress)
            cur_b = init_b + (gap_b * fractional_progress)
            cur_col = self.set_rgb(cur_r, cur_g, cur_b)
            sleep(0.02)  # 20ms

        # And fix it to the target in case float calcs put us off a bit
        return self.set_rgb(r, g, b)

    def set(self, r=None, g=None, b=None, fade=False):
        """
        Sets the LEDs to the specified colour
        Can provide an RGB tuple, RGB separately
        
        @keyword fade: <float> if provided, will make the colour transition smooth over the specified period of time
        """

        # Has a tuple been provided, or comma string?
        if r and isinstance(r, (tuple, list)):
            r, g, b = r  # Unpack
        else:
            try:
                r, g, b = str(r).split(",", 3)
            except ValueError:
                pass
        try:
            r = int(r)
            g = int(g)
            b = int(b)
        except (ValueError, TypeError):
            logging.info("WARNING: no colour identified by '%s'. Using current colour." % r)
            return self.rgb

        if fade:
            return self.fade_to_rgb(r, g, b, fade=fade)
        else:
            return self.set_rgb(r, g, b)

    def fade(self, r=None, g=None, b=None, fade_time=300):
        """
        Fades to the specified colour
        """
        return self.set(r, g, b, fade=fade_time)

    def off(self, *args, **kwargs):
        """
        Fades all channels off
        """
        return self.fade(0, 0, 0)

    def fast_off(self, *args, **kwargs):
        """
        Turns all channels off
        """
        return self.set(0, 0, 0, fade=False)

    def teardown(self):
        """
        Nukes any remaining threads. Called when the parent reactor loop stops
        """
        logging.info("\tLEDstrip: exiting sequence threads...")

        self.parser.set('state', 'r', str(self.red))
        self.parser.set('state', 'g', str(self.green))
        self.parser.set('state', 'b', str(self.blue))
        self.parser.set('state', 'h', str(self.hue))
        self.parser.set('state', 's', str(self.sat))
        self.parser.set('state', 'v', str(self.value))

        with open(self.state_path, 'w') as f:
            self.parser.write(f)

        self.off()  # Fades to black
        logging.info("\t\t...done")
