# MQLed #

Raspberry Pi driven RGB\[W] LED strips

## What is this? ##
MQLed is a Python based controller for LED strips. It allows you to drive full-colour (RGBW) LED strip from a Raspberry Pi, by sending a MQTT messages.

Based on [a tutorial from David Ordnung](https://dordnung.de/raspberrypi-ledstrip/): https://dordnung.de/raspberrypi-ledstrip/

AND [Raspiled](https://github.com/michaeljtbrooks/raspiled/)

*Disclaimer: I am not responsible if you nuke your Raspberry Pi. Do this at your own risk.*

## Hardware requirements ##
1. A network capable Raspberry Pi or Pi Zero W
2. Circuit described in Raspiled project

## Software Requirements ##
1. Python & this repository
2. [Pigpio](http://abyz.me.uk/rpi/pigpio/index.html) to provide you with software pulse width modulation
3. MQTT for Linux (mosquitto)

## Software Installation ##
1. Get Raspian or Ubuntu running on your Raspberry Pi, with network connectivity working, and install essential packages:
   ```bash
   sudo apt-get install build-essential unzip wget git
   ```
2. Install pigpio
   ```
   sudo apt-get install pigpio
   ```
   Enable
   ```
   sudo systemctl enable pigpiod
   ```
   Start
   ```
   sudo systemctl start pigpiod
   ```
   >How about libgpiod:
   >```
   >sudo apt install python3-libgpiod
   >```
   >No. libgpiod does not provide PWM interface
3. Mosquitto

   Install and start
   ```
   sudo apt install -y mosquitto mosquitto-clients
   sudo systemctl enable mosquitto.service
   sudo systemctl start mosquitto.service
   sudo systemctl status mosquitto.service
   ```
   Test
   ```
   mosquitto_sub -t test
   ```
   ```
   mosquitto_pub -t test -m Hello
   ```
4. Download this *MQLed* repo to your Raspberry Pi
5. SSH into your Raspberry Pi. Change to the directory where you saved this repo
6. Install python virtual environments
   ```bash
   sudo apt-get install -y python3-venv
   ```
7. Create a virtual environment to run MQLed in (it installs dependencies - may take mins on Pi)
   ```bash
   ./create_env.sh
   ```
   >It will create virtual environment in ENV directory
8. Find out your Raspberry Pi's IP address:
   ```bash
   ifconfig
   ```
9. Modify ./src/mqled.conf accordingly. PI_PORT should be left as 8888 as this is what Pigpiod is configured to use.
10. Run the mqled server:
      ```bash
      ./ENV/bin/python ./src/mqled.py
      ```
      Test
      ```
      mosquitto_pub -t ledcontrol/strip0 -m 255,255,255
      ```
      ```
      mosquitto_sub -t ledcontrol/strip0color
      ```
      >If run from other host add option -h with IP of Raspberry PI
11. Optional stuff
      If you want the Raspberry Pi to boot up and automatically run MQLed, you can add this command to /etc/rc.local:
      ```bash
      /path/to/your/ENV/bin/python /path/to/your/mqled/src/mqled.py
      ```
      Use absolute paths. e.g. assuming you put MQLed into the /opt directory:
      ```bash
         /opt/mqled/ENV/bin/python  /opt/mqled/src/mqled.py
      ```
      OR
      https://github.com/torfsen/python-systemd-tutorial

      Modify and copy `mqled.service` to `~/.config/systemd/user/`
      ```
      systemctl --user daemon-reload
      ```
      Start
      ```
      systemctl --user start mqled
      ```
      Start at boot
      ```
      systemctl --user enable mqled
      sudo loginctl enable-linger $USER
      ```


## With thanks to ##
* David Ordnung for his [amazing tutorial on driving LED strips from the GPIO pins](https://dordnung.de/raspberrypi-ledstrip/)
* Raspiled's author
