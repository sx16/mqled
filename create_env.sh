#/bin/sh -e
ENVDIR=$(dirname $0)/ENV
python3 -m venv $ENVDIR

. $ENVDIR/bin/activate

pip install -r ./requirements.txt